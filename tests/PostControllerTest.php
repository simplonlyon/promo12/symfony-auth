<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;

class PostControllerTest extends WebTestCase
{

    public function testShowAllArticles()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Blog');
        
        $this->assertSelectorExists('.post');

        $this->assertEquals('16', $crawler->filter('.post')->count());
        $this->assertSelectorTextContains('.post', 'Title 1');
    }

    public function testShowMyPost() {
        $client = static::createClient([], [
            'PHP_AUTH_USER' => 'mail1@mail.com',
            'PHP_AUTH_PW' => '1234'
        ]);

        $crawler = $client->request('GET', '/user/post');

        $this->assertResponseIsSuccessful();
        $this->assertCount(4, $crawler->filter('.post'));

        foreach($crawler->filter('.post') as $post) {
            $this->assertContains('mail1@mail.com', $post->textContent);
        }

/*
        $crawler->filter('.post')->each(function($post) {
            $this->assertContains('mail1@mail.com', $post->text());
        });
*/
    }

    public function testNotAllowed() {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/post');

        $this->assertResponseRedirects();
    }
}
