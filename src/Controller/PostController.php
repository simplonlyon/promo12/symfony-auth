<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/", name="post")
     */
    public function index(PostRepository $repo)
    {
        return $this->render('post/index.html.twig', [
            'posts' => $repo->findAll()
        ]);
    }

    /**
     * @Route("/add-post", name="add_post")
     */
    public function addPost(Request $request, ObjectManager $manager) {
        $post = new Post();

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /* On peut accéder au user actuellement connecté depuis
            les controllers en utilisant la méthode getUser()
            qui nous renvoie une instance de l'entité User, on peut
            donc accéder à tous les getters/setter de ce user
            */
            $post->setAuthor($this->getUser());
            $manager->persist($post);
            $manager->flush();

        }

        return $this->render('post/add.html.twig', [
            'form' => $form->createView()
        ]);

    }
    /**
     * @Route("/user/post", name="user_post")
     */
    public function myPost() {

        return $this->render("post/user.html.twig", [
        ]);
    }
}


